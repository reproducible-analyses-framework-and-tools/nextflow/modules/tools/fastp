#!/usr/bin/env nextflow

process fastp {
// Runs fastp
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - FASTQ Prefix
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq2) - FASTQ 2
//   val parstr - Parameter String
//
// output:
//   tuple => emit: procd_fqs
//     val(pat_name) - Patient Name
//     val(run) - FASTQ Prefix
//     val(dataset) - Dataset
//     path("${dataset}-${pat_name}-${run}*_1*.trimmed.f*q.gz") - Trimmed FASTQ 1
//     path("${dataset}-${pat_name}-${run}*_2*.trimmed.f*q.gz") - Trimmed FASTQ 2
//   path("meta") - Metadata File

// require:
//   FQS
//   params.trim_galore$trim_galore_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'fastp_container'
  label 'fastp'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/fastp"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}*_1*.trimmed.f*q.gz"), path("${dataset}-${pat_name}-${run}*_2*.trimmed.f*q.gz"), emit: procd_fqs
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}.fastp.json"), emit: fq_stats

  script:
  """
  fastp \
  --in1 ${fq1} --out1 ${dataset}-${pat_name}-${run}_1.trimmed.fq.gz \
  --in2 ${fq2} --out2 ${dataset}-${pat_name}-${run}_2.trimmed.fq.gz \
  --failed_out ${dataset}-${pat_name}-${run}.fastp_fails.fq.gz \
  --thread ${task.cpus} \
  --json ${dataset}-${pat_name}-${run}.fastp.json \
  ${parstr}
  """
}
